# RockyLinux with icewm vnc/novnc

Running RockyLinux / icewm accessed with vnc/novnc (web based)

## Getting started

- [ ] Install [Docker for Desktop](https://docs.docker.com/get-docker/) on your local PC or install Docker on a server
- [ ] Generate a ssh public key if required:
```
ssh-keygen -t rsa -b 4096
```
- [ ] Edit **authorized_keys**: add your ssh public key (~/.ssh/id_rsa.pub) at the end of the file
- [ ] From a bash-shell run:

```
cd docker
./build-docker.sh
```

- [ ] then once the image has been build, update the web access, ssh access ports and size of the screen on file run-docker.sh:
- WEBPORT (default is 2443)
- SSHPORT (default is 2022)
- GEOMETRY (default is 1320x720)


```
cd docker
./run-docker.sh
```
 

## Access the container running on your local PC:

- [ ] Access with a local browser at : http://localhost:2080 or http://localhost:2080?resize=remote
- [ ] The default password is password. Please update it with the command **vncpasswd**
- [ ] Access the container ssh with:
```
ssh administrator@localhost -p 2022
```

## Access the container running on an external server through a ssh tunnel:
- [ ] Use PowerShell or bash-shell to run:
```
ssh -L 2080:localhost:2080 'administrator@external_server'

```
