#!/bin/bash
set -e

eksctl create cluster 'smart-factory-1-dev'

aws eks --region us-east-2 update-kubeconfig --name smart-factory-1-dev

eksctl create fargateprofile \
    --cluster  smart-factory-1-dev \
    --name rocky-fargate \
    --namespace rocky

kubectl apply -f namespace.yml

kubectl config set-context --current --namespace=rocky
