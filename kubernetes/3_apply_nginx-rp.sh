#!/bin/bash

kubectl create configmap certpem --from-file=./pem-files/certpem
kubectl create configmap keypem --from-file=./pem-files/keypem

kubectl apply -f nginx-rp.yml
