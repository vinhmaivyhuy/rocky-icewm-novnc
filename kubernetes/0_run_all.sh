#!/bin/bash

kubectl apply -f stateful.yml
kubectl apply -f nodeports.yml
kubectl apply -f rocky-nginx-rp.yml
kubectl apply -f lb-nginx.yml
