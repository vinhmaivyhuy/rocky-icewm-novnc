#! /bin/bash

kubectl create secret docker-registry regcred \
  --docker-server=985324687195.dkr.ecr.us-east-2.amazonaws.com \
  --docker-username=AWS \
  --docker-password=$(aws ecr get-login-password) \
  --namespace=rocky
