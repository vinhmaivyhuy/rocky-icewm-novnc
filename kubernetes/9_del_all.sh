#!/bin/bash

kubectl delete -f lb-nginx.yml
kubectl delete -f rocky-nginx-rp.yml
kubectl delete -f nodeports.yml
kubectl delete -f stateful.yml
