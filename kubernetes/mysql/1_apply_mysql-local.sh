#!/bin/bash
set -e

kubectl apply -f 0_mysql-secret.yml
kubectl apply -f 1_mysql-storage.local.yml 
kubectl apply -f 2_mysql-deployment.yml
