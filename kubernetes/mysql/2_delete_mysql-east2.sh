#!/bin/bash
set -e

kubectl delete -f 2_mysql-deployment.yml
kubectl delete -f 1_mysql-storage.efs-east2.yml
kubectl delete -f 0_mysql-secret.yml
