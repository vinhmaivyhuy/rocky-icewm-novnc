#! /bin/bash
set -e

IMAGE=`cat docker.name`
WEBPORT=10443
SSHPORT=10022
TODAY=$(date +'%Y%m%d')
CURRENT_RUNNING=$(docker container ls | grep -i $IMAGE | awk '{print $1}')

# Stop, save and commit current running container
if [ ! -z "$CURRENT_RUNNING" ]
then
	docker stop $CURRENT_RUNNING 
	docker commit $CURRENT_RUNNING $IMAGE.$TODAY
	docker rm $CURRENT_RUNNING 
fi

#Start new container
docker run -d --name $IMAGE -p $WEBPORT:443 -p $SSHPORT:22 -h $IMAGE $IMAGE:latest
