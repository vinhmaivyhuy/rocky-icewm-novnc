#
# RockyLinux IceWM Dockerfile
#
# Pull base image.
# or run docker pull rockylinux
FROM rockylinux/rockylinux

# Setup argument default and enviroment variables
ARG WEBUSERNAME=rockadmin

ENV WEBUSERNAME ${WEBUSERNAME}

# Update the package manager and upgrade the system
# #################################################
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y upgrade
RUN yum -y install net-tools sudo which vim 
RUN yum -y install tini supervisor procps bind-utils
RUN yum -y install openssh-server openssh-clients
RUN yum -y install langpacks-en glibc-langpack-en
RUN ssh-keygen -A

# Set locale
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8


RUN /bin/dbus-uuidgen > /etc/machine-id
RUN yum -y install nginx
#
#
RUN yum clean all
#

# Setup Supervisord
# #################################################
COPY ./local/supervisord.conf /etc/supervisord.conf
COPY ./supervisord.d/ /etc/supervisord.d/
COPY ./etc-nginx/ /etc/nginx/

# Set up User administrator for ssh
# #################################################
RUN useradd -s /bin/bash -m -b /home administrator
RUN echo "administrator  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/administrator
RUN mkdir /home/administrator/.ssh
COPY authorized_keys /home/administrator/.ssh/authorized_keys
COPY ./local/selfpem /home/administrator/.ssh/selfpem
COPY ./local/certpem /home/administrator/.ssh/certpem
COPY ./local/keypem /home/administrator/.ssh/keypem
RUN chown -R administrator:administrator /home/administrator/.ssh
RUN chmod -R go-rwx /home/administrator/.ssh

# Set up Web User (${WEBUSERNAME})
# #################################################
RUN useradd -s /bin/bash ${WEBUSERNAME}

# Finalize installation and default command
# #################################################
COPY ./local/run.sh /root/run.sh
RUN chmod +x /root/*.sh
RUN rm -f /run/nologin

# Expose ports.
EXPOSE 443
EXPOSE 22

# Define default command
CMD ["/root/run.sh"]
